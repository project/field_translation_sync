# Field translation synchronize

---------------

## About this Module
If you have a multilingual site that has multiple languages for different
countries, you soon run into the issue where Columbian Spanish, Argentinian
Spanish and Mexico Spanish are similar enough that it doesn't make sense to have
3 different translations. You want to add the translation on one of those sites,
then copy it over to all the others. You want to copy it over, because you still
add some customization to the translation after copied into the country. This
module lets you bulk copy/paste from any language into any other language on a
field-by-field basis.


### Installing the Field translation synchronize module

Note: Use of the module on a Composer managed site is also supported.

1. Copy/upload the module to the modules directory of your Drupal installation.

1. Enable the module in 'Extend' (/admin/modules).

1. This module extends Core's Action module. For nodes, you can just add a Node
   operations bulk form to the view. Once you do that, you'll have the option
   to use the `Synchronize field translations: Content` action.
