<?php

namespace Drupal\field_translation_sync;

use Drupal\Core\Entity\ContentEntityInterface;

trait TranslationCrudTrait {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The content translation manager.
   *
   * @var \Drupal\content_translation\ContentTranslationManager
   */
  protected $contentTranslationManager;

  /**
   * Get the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected function getEntityTypeManager() {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::entityTypeManager();
    }
    return $this->entityTypeManager;
  }

  /**
   * Get the content translation manager.
   *
   * @return \Drupal\content_translation\ContentTranslationManager
   */
  protected function getContentTranslationManager() {
    if (!isset($this->contentTranslationManager)) {
      $this->contentTranslationManager = \Drupal::service('content_translation.manager');
    }
    return $this->contentTranslationManager;
  }

  /**
   * Populates target values with the source values.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being translated.
   * @param string $source_langcode
   *   The language to be used as source.
   * @param string $target_langcode
   *   The language to be used as target.
   */
  protected function prepareTranslation(ContentEntityInterface $entity, $source_langcode, $target_langcode) {
    /* @var \Drupal\Core\Entity\ContentEntityInterface $source_translation */
    $source_translation = $entity->getTranslation($source_langcode);
    $target_translation = $entity->addTranslation($target_langcode, $source_translation->toArray());

    // Make sure we do not inherit the affected status from the source values.
    if ($entity->getEntityType()->isRevisionable()) {
      $target_translation->setRevisionTranslationAffected(NULL);
    }

    /** @var \Drupal\user\UserInterface $user */
    $user = $this->getEntityTypeManager()->getStorage('user')->load($this->currentUser->id());
    $metadata = $this->getContentTranslationManager()->getTranslationMetadata($target_translation);

    // Update the translation author to current user, as well the translation
    // creation time.
    $metadata->setAuthor($user);
    $metadata->setCreatedTime($this->time->getRequestTime());
    $metadata->setSource($source_langcode);
  }

}
