<?php

namespace Drupal\field_translation_sync\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The translation sync form.
 */
class TranslationSyncForm extends ConfirmFormBase {

  /**
   * Private temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * Count of entities to modify.
   *
   * @var int
   */
  protected $count = 0;

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * TranslationSyncForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   Temp store service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundleInfo
   *   Bundle info object.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityRepositoryInterface $entityRepository, PrivateTempStoreFactory $temp_store_factory, TimeInterface $time, AccountInterface $currentUser, EntityTypeBundleInfoInterface $bundleInfo, EntityFieldManagerInterface $entityFieldManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityRepository = $entityRepository;
    $this->tempStore = $temp_store_factory->get('field_translation_sync');
    $this->time = $time;
    $this->currentUser = $currentUser;
    $this->bundleInfo = $bundleInfo;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity.repository'),
      $container->get('tempstore.private'),
      $container->get('datetime.time'),
      $container->get('current_user'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_translation_sync_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $bundle_data = [];
    foreach ($this->getEntityData() as $entity_type_id => $bundle_entities) {
      $bundle_info = $this->bundleInfo->getBundleInfo($entity_type_id);
      foreach ($bundle_entities as $bundle => $entities) {
        $this->count += count($entities);
        $bundle_data[$entity_type_id][$bundle] = $bundle_info[$bundle]['label'];
      }
    }

    $form['source_language'] = [
      '#type' => 'language_select',
      '#title' => $this->t('The source language.'),
    ];
    $form = $this->buildBundleForms($form, $form_state, $bundle_data);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->submitConfigurationForm($form, $form_state);
    foreach ($this->getEntityData() as $entity_type_id => $bundle_entities) {
      foreach ($bundle_entities as $bundle => $entities) {
        $loaded_entities = $this->entityTypeManager->getStorage($entity_type_id)
          ->loadMultiple(array_keys($entities));
        foreach ($loaded_entities as $entity) {
          foreach ($entities[$entity->id()] as $langcode) {
            $this->execute($this->entityRepository->getTranslationFromContext($entity, $langcode));
          }
        }
      }
    }

    $this->clearEntityData();
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->formatPlural($this->count, 'Are you sure you want to synchronize the translation for this (@count) entity?', 'Are you sure you want to synchronize the translations for these (@count) entities?', );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('<front>');
  }

  /**
   * Gets the saved entity data.
   *
   * @return array
   *   An array of saved entity data.
   */
  protected function getEntityData() {
    return $this->tempStore->get($this->currentUser->id()) ?: [];
  }

  /**
   * Clear the saved entities once we've finished with them.
   */
  protected function clearEntityData() {
    $this->tempStore->delete($this->currentUser->id());
  }

  /**
   * Builds the bundle forms.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param array $bundle_data
   *   An array with all entity types and their bundles.
   *
   * @return array
   *   The bundle forms.
   */
  public function buildBundleForms(array $form, FormStateInterface $form_state, array $bundle_data) {

    // Store entity data.
    $form_state->set('fts_entity_bundles_data', $bundle_data);

    $bundle_count = 0;
    foreach ($bundle_data as $entity_type_id => $bundles) {
      foreach ($bundles as $bundle => $label) {
        $bundle_count++;
      }
    }

    foreach ($bundle_data as $entity_type_id => $bundles) {
      foreach ($bundles as $bundle => $label) {
        $form = $this->getBundleForm($entity_type_id, $bundle, $label, $form, $form_state, $bundle_count);
      }
    }

    return $form;
  }

  /**
   * Gets the form for this entity display.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle
   *   The bundle ID.
   * @param mixed $bundle_label
   *   Bundle label.
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state object.
   * @param int $bundle_count
   *   Number of bundles that may be affected.
   *
   * @return array
   *   Edit form for the current entity bundle.
   */
  protected function getBundleForm($entity_type_id, $bundle, $bundle_label, array $form, FormStateInterface $form_state, $bundle_count) {
    $entityType = $this->entityTypeManager->getDefinition($entity_type_id);
    $entity = $this->entityTypeManager->getStorage($entity_type_id)->create([
      $entityType->getKey('bundle') => $bundle,
    ]);

    if (!isset($form[$entity_type_id])) {
      $form[$entity_type_id] = [
        '#type' => 'container',
        '#tree' => TRUE,
      ];
    }

    // If there is no bundle label, the entity has no bundles.
    if (empty($bundle_label)) {
      $bundle_label = $entityType->getLabel();
    }
    $form[$entity_type_id][$bundle] = [
      '#type' => 'details',
      '#open' => ($bundle_count === 1),
      '#title' => $entityType->getLabel() . ' - ' . $bundle_label,
      '#parents' => [$entity_type_id, $bundle],
    ];

    $form_display = EntityFormDisplay::collectRenderDisplay($entity, 'field_translation_sync');
    $form_display->buildForm($entity, $form[$entity_type_id][$bundle], $form_state);

    $form[$entity_type_id][$bundle] += $this->getSelectorForm($entity_type_id, $bundle, $form[$entity_type_id][$bundle]);

    return $form;
  }

  /**
   * Builds the selector form.
   *
   * Given an entity form, create a selector form to provide options to update
   * values.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $bundle
   *   The bundle machine name.
   * @param array $form
   *   The form we're building the selection options for.
   *
   * @return array
   *   The new selector form.
   */
  protected function getSelectorForm($entity_type_id, $bundle, array &$form) {
    $selector['field_selectors'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Select fields to synchronize'),
      '#weight' => -50,
      '#tree' => TRUE,
    ];

    $definitions = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle);

    foreach (Element::children($form) as $key) {
      if (isset($form[$key]['#access']) && !$form[$key]['#access']) {
        continue;
      }
      if (
        (!$element = &$this->findFormElement($form[$key])) ||
        !$definitions[$key]->isTranslatable() ||
        $definitions[$key]->getType() === 'language' ||
        !$definitions[$key]->isDisplayConfigurable('form')
      ) {
        // Force the form element to be hidden.
        unset($form[$key]);
        continue;
      }
      // Force the form element to be hidden.
      unset($form[$key]);

      // Add the toggle field to the form.
      $selector['field_selectors'][$key] = [
        '#type' => 'checkbox',
        '#title' => $element['#title'],
        '#weight' => $form[$key]['#weight'] ?? 0,
        '#tree' => TRUE,
      ];
    }

    if (empty(Element::children($selector['field_selectors']))) {
      $selector['field_selectors']['#title'] = $this->t('There are no fields available to modify');
    }

    return $selector;
  }

  /**
   * Finds the deepest most form element and returns it.
   *
   * @param array $form
   *   The form element we're searching.
   * @param string $title
   *   The most recent non-empty title from previous form elements.
   *
   * @return array|null
   *   The deepest most element if we can find it.
   */
  protected function &findFormElement(array &$form, $title = NULL) {
    $element = NULL;
    foreach (Element::children($form) as $key) {
      // Not all levels have both #title and #type.
      // Attempt to inherit #title from previous iterations.
      // Some #titles are empty strings.  Ignore them.
      if (!empty($form[$key]['#title'])) {
        $title = $form[$key]['#title'];
      }
      elseif (!empty($form[$key]['title']['#value']) && !empty($form[$key]['title']['#type']) && $form[$key]['title']['#type'] === 'html_tag') {
        $title = $form[$key]['title']['#value'];
      }
      if (isset($form[$key]['#type']) && !empty($title)) {
        // Fix empty or missing #title in $form.
        if (empty($form[$key]['#title'])) {
          $form[$key]['#title'] = $title;
        }
        $element = &$form[$key];
        break;
      }
      elseif (is_array($form[$key])) {
        $element = &$this->findFormElement($form[$key], $title);
      }
    }
    return $element;
  }

  /**
   * Save field translation sync details to action configuration.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state object.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['source_language'] = $form_state->getValue('source_language');
    foreach ($form_state->getStorage()['fts_entity_bundles_data'] as $entity_type_id => $bundles) {
      foreach ($bundles as $bundle => $label) {
        $this->configuration[$entity_type_id][$bundle]['fields'] = array_filter($form_state->getValue([$entity_type_id, $bundle, 'field_selectors']));
      }
    }
  }

  /**
   * Executes the translation sync.
   *
   * @param ContentEntityInterface $entity
   *   The content entity.
   */
  protected function execute(ContentEntityInterface $entity) {
    $type_id = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    $source_language_entity = $this->entityRepository->getTranslationFromContext($entity, $this->configuration['source_language']);
    if (isset($this->configuration[$type_id][$bundle])) {
      $fields = array_keys($this->configuration[$type_id][$bundle]['fields']);
      foreach ($fields as $field) {
        $entity->{$field} = $source_language_entity->get($field);
      }
      $entity->save();
    }
  }

}
