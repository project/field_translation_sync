<?php

namespace Drupal\field_translation_sync\Form;

use Drupal\content_translation\Access\ContentTranslationDeleteAccess;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The translation remove form.
 */
class TranslationRemoveForm extends ConfirmFormBase {

  /**
   * Private temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * Count of entities to modify.
   *
   * @var int
   */
  protected $count = 0;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The content translation delete access service.
   *
   * @var \Drupal\content_translation\Access\ContentTranslationDeleteAccess
   */
  protected $translationDeleteAccess;

  /**
   * TranslationRemoveForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   Temp store service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, PrivateTempStoreFactory $temp_store_factory, AccountInterface $currentUser, ContentTranslationDeleteAccess $translation_delete_access) {
    $this->entityTypeManager = $entityTypeManager;
    $this->tempStore = $temp_store_factory->get('remove_translation');
    $this->currentUser = $currentUser;
    $this->translationDeleteAccess = $translation_delete_access;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('tempstore.private'),
      $container->get('current_user'),
      $container->get('content_translation.delete_access')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_translation_sync_remove_translation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['target_language'] = [
      '#type' => 'language_select',
      '#title' => $this->t('The target language.'),
    ];

    foreach ($this->getEntityData() as $entity_type_id => $entity_ids) {
      $this->count += count($entity_ids);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->submitConfigurationForm($form, $form_state);
    foreach ($this->getEntityData() as $entity_type_id => $entity_ids) {
      $loaded_entities = $this->entityTypeManager->getStorage($entity_type_id)
        ->loadMultiple(array_keys($entity_ids));
      foreach ($loaded_entities as $entity) {
        $this->execute($entity);
      }
    }
    $this->clearEntityData();
  }

  /**
   * Save remove translation details to action configuration.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state object.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['target_language'] = $form_state->getValue('target_language');
  }

  /**
   * Executes the translation removal.
   *
   * @param ContentEntityInterface $entity
   *   The content entity.
   */
  protected function execute(ContentEntityInterface $entity) {
    $target_language = $this->configuration['target_language'];
    if ($entity->hasTranslation($target_language) && $this->translationDeleteAccess->checkAccess($entity)) {
      $untranslated_entity = $entity->getUntranslated();
      $untranslated_entity->removeTranslation($target_language);
      $untranslated_entity->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->formatPlural($this->count, 'Are you sure you want to remove a translation for this (@count) entity?', 'Are you sure you want to remove the translations for these (@count) entities?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('<front>');
  }

  /**
   * Gets the saved entity data.
   *
   * @return array
   *   An array of saved entity data.
   */
  protected function getEntityData() {
    return $this->tempStore->get($this->currentUser->id()) ?: [];
  }

  /**
   * Clear the saved entities once we've finished with them.
   */
  protected function clearEntityData() {
    $this->tempStore->delete($this->currentUser->id());
  }

}
