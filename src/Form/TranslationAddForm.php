<?php

namespace Drupal\field_translation_sync\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\content_translation\ContentTranslationManager;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\field_translation_sync\TranslationCrudTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The translation add form.
 */
class TranslationAddForm extends ConfirmFormBase {

  use TranslationCrudTrait;

  /**
   * Private temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * Count of entities to modify.
   *
   * @var int
   */
  protected $count = 0;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * TranslationAddForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   Temp store service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\content_translation\ContentTranslationManager $content_translation_manager
   *   The content translation manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, PrivateTempStoreFactory $temp_store_factory, AccountInterface $currentUser, ContentTranslationManager $content_translation_manager, TimeInterface $time) {
    $this->entityTypeManager = $entityTypeManager;
    $this->tempStore = $temp_store_factory->get('add_translation');
    $this->currentUser = $currentUser;
    $this->contentTranslationManager = $content_translation_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('tempstore.private'),
      $container->get('current_user'),
      $container->get('content_translation.manager'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_translation_sync_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['source_language'] = [
      '#type' => 'language_select',
      '#title' => $this->t('The source language.'),
    ];
    $form['target_language'] = [
      '#type' => 'language_select',
      '#title' => $this->t('The target language.'),
    ];

    foreach ($this->getEntityData() as $entity_type_id => $entity_ids) {
      $this->count += count($entity_ids);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->submitConfigurationForm($form, $form_state);
    foreach ($this->getEntityData() as $entity_type_id => $entity_ids) {
      $loaded_entities = $this->getEntityTypeManager()->getStorage($entity_type_id)
        ->loadMultiple(array_keys($entity_ids));
      foreach ($loaded_entities as $entity) {
        $this->execute($entity);
      }
    }
    $this->clearEntityData();
  }

  /**
   * Save field translation sync details to action configuration.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state object.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['source_language'] = $form_state->getValue('source_language');
    $this->configuration['target_language'] = $form_state->getValue('target_language');
  }

  /**
   * Executes the translation addition.
   *
   * @param ContentEntityInterface $entity
   *   The content entity.
   */
  protected function execute(ContentEntityInterface $entity) {
    $source_language = $this->configuration['source_language'];
    $target_language = $this->configuration['target_language'];
    if ($entity->hasTranslation($source_language) && !$entity->hasTranslation($target_language)) {
      $this->prepareTranslation($entity, $source_language, $target_language);
      $entity->getTranslation($target_language)->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->formatPlural($this->count, 'Are you sure you want to add a translation for this (@count) entity?', 'Are you sure you want to add the translations for these (@count) entities?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('<front>');
  }

  /**
   * Gets the saved entity data.
   *
   * @return array
   *   An array of saved entity data.
   */
  protected function getEntityData() {
    return $this->tempStore->get($this->currentUser->id()) ?: [];
  }

  /**
   * Clear the saved entities once we've finished with them.
   */
  protected function clearEntityData() {
    $this->tempStore->delete($this->currentUser->id());
  }

}
