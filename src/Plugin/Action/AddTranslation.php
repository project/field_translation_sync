<?php

namespace Drupal\field_translation_sync\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Add translation.
 *
 * @Action(
 *   id = "add_translation",
 *   action_label = @Translation("Add translations: "),
 *   confirm_form_route_name = "field_translation_sync.add_translation_form",
 *   deriver = "Drupal\field_translation_sync\Plugin\Action\Derivative\TranslationDeriver"
 * )
 */
class AddTranslation extends TranslationBase {

  /**
   * {@inheritdoc}
   */
  protected $tempstore_name = 'add_translation';

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    /** @var \Drupal\Core\Entity\EntityInterface[] $entities */
    $selection = [];
    foreach ($entities as $entity) {
      $selection[$entity->getEntityTypeId()][$entity->id()] = $entity->id();
    }
    $this->tempStore->set($this->currentUser->id(), $selection);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $entity_type_id = $object->getEntityTypeId();
    $bundle = $object->bundle();
    $allowed = $account->hasPermission("translate $entity_type_id") || $account->hasPermission("translate $bundle $entity_type_id");
    return $return_as_object ? AccessResult::allowedIf($allowed) : $account;
  }

}
