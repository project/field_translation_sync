<?php

namespace Drupal\field_translation_sync\Plugin\Action;

use Drupal\Core\Session\AccountInterface;

/**
 * Synchronize field translations.
 *
 * @Action(
 *   id = "synchronize_field_translations",
 *   action_label = @Translation("Synchronize field translations: "),
 *   confirm_form_route_name = "field_translation_sync.sync_form",
 *   deriver = "Drupal\field_translation_sync\Plugin\Action\Derivative\TranslationDeriver"
 * )
 */
class SynchronizeFieldTranslations extends TranslationBase {

  protected $tempstore_name = 'field_translation_sync';

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    /** @var \Drupal\Core\Entity\EntityInterface[] $entities */
    $selection = [];
    foreach ($entities as $entity) {
      $langcode = $entity->language()->getId();
      $selection[$entity->getEntityTypeId()][$entity->bundle()][$entity->id()][$langcode] = $langcode;
    }
    $this->tempStore->set($this->currentUser->id(), $selection);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $object->access('update', $account, $return_as_object);
  }

}
