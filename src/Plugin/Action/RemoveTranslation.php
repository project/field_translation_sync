<?php

namespace Drupal\field_translation_sync\Plugin\Action;

use Drupal\content_translation\Access\ContentTranslationDeleteAccess;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Remove translation.
 *
 * @Action(
 *   id = "remove_translation",
 *   action_label = @Translation("Remove translations: "),
 *   confirm_form_route_name = "field_translation_sync.remove_translation_form",
 *   deriver = "Drupal\field_translation_sync\Plugin\Action\Derivative\TranslationDeriver"
 * )
 */
class RemoveTranslation extends TranslationBase {

  /**
   * {@inheritdoc}
   */
  protected $tempstore_name = 'remove_translation';

  /**
   * The content translation delete access service.
   *
   * @var \Drupal\content_translation\Access\ContentTranslationDeleteAccess
   */
  protected $translationDeleteAccess;

  /**
   * Constructs a new RemoveTranslation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\content_translation\Access\ContentTranslationDeleteAccess $translation_delete_access
   *   The translation delete access service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PrivateTempStoreFactory $temp_store_factory, AccountInterface $current_user, ContentTranslationDeleteAccess $translation_delete_access) {
    $this->translationDeleteAccess = $translation_delete_access;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $temp_store_factory, $current_user);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('tempstore.private'),
      $container->get('current_user'),
      $container->get('content_translation.delete_access')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    /** @var \Drupal\Core\Entity\EntityInterface[] $entities */
    $selection = [];
    foreach ($entities as $entity) {
      $selection[$entity->getEntityTypeId()][$entity->id()] = $entity->id();
    }
    $this->tempStore->set($this->currentUser->id(), $selection);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = $this->translationDeleteAccess->checkAccess($object);
    return $return_as_object ? $result : $result->isAllowed();
  }

}
